# sais-pas-coder

On va utiliser python pour faire un snake ! (pun intended)

## Préparer le terrain

### Installer python: 
Vas sur le site de python et télécharge la dernière version:

https://www.python.org/ftp/python/3.8.2/python-3.8.2.exe

Python c'est le langage de programmation qu'on va utiliser. J'ai choisi python
pour sa clarté et sa facilité d'utilisation afin de ne pas perdre de temps.

### Installer pygame:
Ensuite on va avoir besoin d'une librairie qui nous fournit les outils pour
dessiner des choses à l'écran. De base avec python on ne peut faire que des
applications dans la console et c'est pas très beau.

`python3 -m pip install -U pygame --user`

Il faut maintenant véridier que pygame fonction. Fais ça en vérifiant que le jeu
se lance avec la commande suivante: 

`python3 -m pygame.examples.aliens`

## On entre dans le vif du sujet

### Crash-course de python

#### Commentaires
```python
# Ceci est un commentaire
```

Assigner une variable:
```python
variable = valeur
```

#### Listes

Créer une liste vide:
```python
ma_liste = []
```

Ajouter un élément à la liste:
```python
ma_liste.append(nouvel_element)
```

Accéder au i-ème élément d'une liste (**commençant à 0 !**)
```python
element = ma_liste[i]
```

#### Conditions

Condition:
```python
if condition:
    #code si vrai
```

Condition avec cas "sinon":
```python
if condition:
    #code si condition est vraie
else:
    #code sinon
```

Condition avec plus de branches:
```python
if condition_1:
    # code si condition 1 est vraie
elif condition_2:
    # code si condition 1 est fausse mais condition 2 est vraie
else:
    # code sinon
```

Opérateurs logique ET:
```python
condition_1 and condition_2
```

#### Fonctions

Définir une fonction:
```python
def ma_fonction(mes_parametres):
    # du code
```

Retourner une valeur d'une fonction:
```python
def ma_fonction(mes_parametres):
    # du code
    return resultat
```

#### Boucles

Répéter tant qu'une condition est respectée:
```python
while condition:
    # instructions à répéter
```

Répéter x fois
```python
for x in range(x):
    # code à répéter x fois
```

Itérer sur chaque valeur dans une liste:
```python
for element in liste:
    # faire un truc avec element
```

### Dessinez c'est gagné !


> C'est bien beau tout ça, mais je ne connais pas pyGame moi ! 

Pas de panique, on aura seulement besoin de quelques fonctions simples pour 
afficher quelque chose sur notre écran.

La première étape c'est de créer une 
fenêtre. Pour ce faire il suffit d'appeler la fonction 
`pygame.display.set_mode((width, height))` et de stocker le résultat dans 
une variable. 

`win = pygame.display.set_mode((width, height))`

Le tuple `(width, height)` permet de
définir la taille de la fenêtre. Je vous avait dit que c'était pas compliqué !

Maintenant ce serait cool de pouvoir changer la couleur de cette fenêtre pour
choisir une couleur de fond plus agréable. PyGame fournit là aussi une fonction
qui permet de faire exactement ça: `win.fill(color)`. Dans cette instruction
`win` est l'objet à colorier (ici notre fenêtre) et `color` est un tuple 
représentant la couleur de remplissage dans le format suivant: 
`(rouge, vert, bleu)` où les valeurs vont de 0 à 255. On va choisir du rouge pour
le moment `(100,0,0)`, mais sens-toi libre de mettre d'expérimenter avec la 
couleur.

> Rien ne s'affiche pour l'instant, j'ai fait un truc faux ?

Non, tout va bien ! En fait, on a seulement dit à PyGame comment peindre la 
fenêtre, mais on ne lui a pas demandé de la peindre vraiment ! Ca se fait très
simplement avec l'instruction `pygame.display.update()`.

Ajoutons un peu de complexité à notre beau fond uni. La forme la plus facile à
dessiner est sans doute un rectangle. Tu l'auras deviné, pyGame nous fournit
évidemment une fonction pour ce faire. Son petit nom c'est 
`pygame.draw.rect(surface, color, (pos_x,pos_y, largeur, hauteur))`. Elle
dessine un rectangle sur la `surface` de couleur `color` aux coordonnées
`pos_x, pos_y`, de largeur `largeur` et de hauteur `hauteur`. Ajoute un carré
dans ta fenêtre et n'oublie pas de placer cet appel entre l'initialisation de
la fenêtre et l'instruction qui demande à pyGame de dessiner les choses à 
l'écran.

> Trop cool ! Mais maintenant comment on fait bouger des choses ?

Une chose à la fois ! On va tout d'abord étudier un peu l'anatomie du code d'un
jeu vidéo. Comme tu le sais peut-être, une animation est simplement constituée
d'une suite d'images. Ce qu'on va faire est donc ni plus ni moins que de
dessiner rapidement des images.